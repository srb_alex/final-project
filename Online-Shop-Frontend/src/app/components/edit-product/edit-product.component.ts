import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  productForm = new FormGroup({
    id: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    imageUrl: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    unitPrice: new FormControl('', Validators.required),
    categoryId: new FormControl('', Validators.required),
  });

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const productId = this.route.snapshot.params.id;

    this.productService.getOne(productId).subscribe(
      response => {
        this.productForm.patchValue({
          id: response.id,
          description: response.description,
          imageUrl: response.imageUrl,
          name: response.name,
          unitPrice: response.unitPrice,
          categoryId: response.category.id
        });

      });

  }

  update() {
    this.productService.updateProduct(this.productForm.value).subscribe(response => {
      console.log(response)
      this.productForm.reset();
    }, () => {
      alert('ERROR: Failed to save product.')
    });
  }

}
