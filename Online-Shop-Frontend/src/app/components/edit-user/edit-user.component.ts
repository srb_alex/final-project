import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  userForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    roles: new FormControl('', Validators.required),
  });

  constructor(private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const userId = +this.route.snapshot.params.id;

    this.auth.getOne(userId).subscribe(
      response => {
        this.userForm.patchValue({
          username: response.username,
          password: response.password,
          roles: response.roles[0].id
        });

      });

  }

  update() {
    this.auth.update(this.userForm.value).subscribe(response => {
      console.log(response)
      this.userForm.reset();
    }, () => {
      alert('ERROR: Failed to save user.')
    });
  }

}
