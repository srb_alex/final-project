import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  users: User[];
  products: Product[];

  constructor(private auth: AuthService, private productService: ProductService) { }

  ngOnInit(): void {
    this.loadAllUsers();
    this.loadAllProducts();
  }

  loadAllUsers() {
    return this.auth.getAll().subscribe(
      data => {
        this.users = data;
      }
    )
  }

  loadAllProducts() {
    return this.productService.getProducts().subscribe(
      data => {
        this.products = data;
      }
    )
  }

  deleteUser(user: User) {
    const ok = confirm(`Are you sure you want to delete this username: ${user.username}?`);

    if (ok) {
      return this.auth.delete(user).subscribe(() => {
        this.loadAllUsers()
      });
    }
  }

  deleteOneProduct(product: Product) {
    const ok = confirm(`Are you sure you want to delete this product: ${product.name}?`);

    if (ok) {
      return this.productService.deleteProduct(product).subscribe(() => {
        this.loadAllUsers()
      });
    }
  }

}



