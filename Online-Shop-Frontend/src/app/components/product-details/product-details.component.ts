import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from 'src/app/models/cart-item';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  product: Product = new Product();

  constructor(private cartService: CartService, private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.handleProductDetails();
    })
  }
  handleProductDetails(): void {

    const theProductId: number = +this.route.snapshot.paramMap.get('id');

    this.productService.getOne(theProductId).subscribe(
      data => {
        this.product = data;
      }
    )
  }

  addToCart() {

    const theCartItem = new CartItem(this.product);

    this.cartService.addToCart(theCartItem);
  }

}
