import { Component, OnInit } from '@angular/core';
import { Categories } from 'src/app/models/categories';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories: Categories[];

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit(): void {
    this.listProductCategories();
  }

  listProductCategories() {
    this.categoriesService.getCategories().subscribe(
      data => {
        this.categories = data;
      }
    )
  }
}
