import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Categories } from 'src/app/models/categories';
import { Product } from 'src/app/models/product';
import { CategoriesService } from 'src/app/services/categories.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  product: Product;
  category: Categories;

  productForm = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    imageUrl: new FormControl('', Validators.required),
    unitPrice: new FormControl('', Validators.required),
    unitsInStock: new FormControl('', Validators.required),
    categoryId: new FormControl('', Validators.required),
  });

  constructor(private productService: ProductService, private categoryService: CategoriesService, private router: Router) { }

  ngOnInit(): void {
  }

  createProduct(category: Categories) {
    const newProduct: Product = {
      id: this.productForm.value.id,
      name: this.productForm.value.name,
      description: this.productForm.value.description,
      imageUrl: this.productForm.value.imageUrl,
      unitPrice: this.productForm.value.unitPrice,
      unitsInStock: this.productForm.value.unitsInStock,
      category: category
    };

    this.productService.addProduct(newProduct).subscribe(
      () => {
        alert('Product saved.');
        this.router.navigateByUrl('/admin')
      });
  }

  onSubmit() {
    console.log(this.productForm.value.categoryId);
    this.categoryService.getCategory(this.productForm.value.categoryId).subscribe(
      response => {
        this.category = response;
        this.createProduct(response);
      });
  }
}
