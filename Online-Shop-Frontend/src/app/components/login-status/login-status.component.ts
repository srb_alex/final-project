import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-status',
  templateUrl: './login-status.component.html',
  styleUrls: ['./login-status.component.css']
})
export class LoginStatusComponent implements OnInit {

  isAuthenticated: boolean = false;
  username: string;
  role: string;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    const userLocalStorage = this.getUser();
    this.username = userLocalStorage.username;
    this.role = userLocalStorage.roles;
    if (this.username !== undefined) {
      this.username;
      this.role;
      this.isAuthenticated = true;
    }
    else {
      this.username = '';
      this.role = '';
      this.isAuthenticated = false;
    }
  }

  public getUser(): any {
    const user = localStorage.getItem('currentUser');
    if (user) {
      return JSON.parse(user);
    }
    return {};
  }

  onLogout() {
    this.authService.logout();
  }
}
