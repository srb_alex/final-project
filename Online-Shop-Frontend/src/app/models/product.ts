import { Categories } from "./categories";

export class Product {
    id: number;
    name: string;
    description: string;
    unitPrice: number;
    imageUrl: string;
    unitsInStock: number;
    category: Categories
}

