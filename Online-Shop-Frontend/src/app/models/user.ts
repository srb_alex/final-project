import { Roles } from './roles';

export class User {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    password: string;
    email: string;
    roles: Roles;
}


