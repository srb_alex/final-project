import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Country } from '../models/country';
import { State } from '../models/state';

@Injectable({
  providedIn: 'root'
})
export class OnlineShopFormService {

  private countriesUrl = `${environment.baseUrl}/countries`;
  private stateUrl = `${environment.baseUrl}/states`;

  constructor(private http: HttpClient) { }

  getCountries(): Observable<Country[]> {

    return this.http.get<Country[]>(this.countriesUrl);
  }

  getStates(theCountryId: number): Observable<State[]> {

    const searchStatesUrl = `${this.stateUrl}/findByCountryId/${theCountryId}`;

    return this.http.get<State[]>(searchStatesUrl);
  }

  getCreditCardMonth(startMonth: number): Observable<number[]> {

    let data: number[] = [];

    for (let theMonth = startMonth; theMonth <= 12; theMonth++) {
      data.push(theMonth);
    }

    return of(data);
  }

  getCreditCardYears(): Observable<number[]> {

    let data: number[] = [];

    const startYear: number = new Date().getFullYear();
    const endYear: number = startYear + 10;

    for (let theYear = startYear; theYear <= endYear; theYear++) {
      data.push(theYear);
    }

    return of(data);
  }
}
