import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User;

  constructor(private http: HttpClient) { }

  register(username: string, password: string) {
    return this.http.post(`${environment.baseUrl}/users/signup`,
      {
        username: username,
        password: password,
      });
  }

  login(username: string, password: string) {
    return this.http.post(`${environment.baseUrl}/users/signin`,
      {
        username: username,
        password: password,
      }).pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        location.reload();
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    location.reload();
  }

  getOne(id: number) {
    return this.http.get<User>(`${environment.baseUrl}/users/${id}`);
  }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.baseUrl}/users`);
  }

  save(newUser: User) {
    return this.http.post<User>(`${environment.baseUrl}/users`, newUser);
  }

  update(user: User) {
    return this.http.put<User>(`${environment.baseUrl}/users/${user.id}`, user);
  }

  delete(user: User) {
    return this.http.delete(`${environment.baseUrl}/users/${user.id}`, { responseType: 'text' });
  }
}


