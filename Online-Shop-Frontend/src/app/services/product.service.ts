import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  product: Product;

  constructor(private http: HttpClient) { }

  getOne(id: number) {
    return this.http.get<Product>(`${environment.baseUrl}/products/${id}`);
  }

  getProductList(theCategoryId: number): Observable<Product[]> {

    return this.http.get<Product[]>(`${environment.baseUrl}/products/searchByCategory${theCategoryId}`);
  }

  searchProducts(theKeyword: string): Observable<Product[]> {

    return this.http.get<Product[]>(`${environment.baseUrl}/products/searchByName/${theKeyword}`);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${environment.baseUrl}/products`);
  }

  addProduct(newProduct: Product) {
    return this.http.post<Product>(`${environment.baseUrl}/products`, newProduct);
  }

  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(`${environment.baseUrl}/products/${product.id}`, product);
  }

  deleteProduct(product: Product): Observable<any> {
    return this.http.delete(`${environment.baseUrl}/products/${product.id}`, { responseType: 'text' });
  }

}
