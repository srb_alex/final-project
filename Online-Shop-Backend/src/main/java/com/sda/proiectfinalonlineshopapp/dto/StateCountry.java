package com.sda.proiectfinalonlineshopapp.dto;

import com.sda.proiectfinalonlineshopapp.models.Country;
import lombok.Data;

@Data
public class StateCountry {

    private Country country;

    private int id;

    private String name;
}
