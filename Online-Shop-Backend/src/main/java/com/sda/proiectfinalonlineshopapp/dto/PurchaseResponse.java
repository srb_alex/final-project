package com.sda.proiectfinalonlineshopapp.dto;

import lombok.Data;

@Data
public class PurchaseResponse {

    private final String orderTrackingNumber;
}
