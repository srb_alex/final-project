package com.sda.proiectfinalonlineshopapp.dto;

import com.sda.proiectfinalonlineshopapp.models.Category;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductCategory {

    private Category category;

    private String name;

    private String description;

    private BigDecimal unitPrice;

    private String imageUrl;

    private int unitsInStock;

}
