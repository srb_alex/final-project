package com.sda.proiectfinalonlineshopapp.dto;

import com.sda.proiectfinalonlineshopapp.models.Address;
import com.sda.proiectfinalonlineshopapp.models.Customer;
import com.sda.proiectfinalonlineshopapp.models.Order;
import com.sda.proiectfinalonlineshopapp.models.OrderItem;
import lombok.Data;

import java.util.Set;

@Data
public class Purchase {

    private Customer customer;

    private Address shippingAddress;

    private Address billingAddress;

    private Order order;

    private Set<OrderItem> orderItems;

}
