package com.sda.proiectfinalonlineshopapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProiectFinalOnlineShopAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProiectFinalOnlineShopAppApplication.class, args);
	}

}
