package com.sda.proiectfinalonlineshopapp.repository;

import com.sda.proiectfinalonlineshopapp.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
