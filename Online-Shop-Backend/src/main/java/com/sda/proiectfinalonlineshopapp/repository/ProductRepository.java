package com.sda.proiectfinalonlineshopapp.repository;

import com.sda.proiectfinalonlineshopapp.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findByCategoryId(@RequestParam("id") Long id);

    List<Product> findByNameContaining(@RequestParam("name") String name);
}

