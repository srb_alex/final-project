package com.sda.proiectfinalonlineshopapp.repository;

import com.sda.proiectfinalonlineshopapp.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
