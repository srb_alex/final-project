package com.sda.proiectfinalonlineshopapp.repository;

import com.sda.proiectfinalonlineshopapp.models.State;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StateRepository extends JpaRepository<State, Integer> {

    List<State> findByCountryId(Integer id);
}
