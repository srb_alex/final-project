package com.sda.proiectfinalonlineshopapp.repository;

import com.sda.proiectfinalonlineshopapp.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository  extends JpaRepository<Country, Integer> {
}
