package com.sda.proiectfinalonlineshopapp.services;

import com.sda.proiectfinalonlineshopapp.dto.Purchase;
import com.sda.proiectfinalonlineshopapp.dto.PurchaseResponse;

public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);
}
