package com.sda.proiectfinalonlineshopapp.controllers;

import com.sda.proiectfinalonlineshopapp.models.Country;
import com.sda.proiectfinalonlineshopapp.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/countries")
public class CountryController {

    @Autowired
    private CountryRepository countryRepository;

    @GetMapping
    public List<Country> getAllCountries() {

       return countryRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Country> getCountry(@PathVariable("id") Integer id) {

        Optional<Country> theCountry = countryRepository.findById(id);

        if(theCountry.isEmpty()) {
         throw new RuntimeException("Country id not found - " +id);
        }

        return theCountry;
    }

    @PostMapping
    public Country addCountry(@RequestBody Country country) {

        countryRepository.save(country);

        return country;
    }

    @PutMapping("/{}")
    public Country updateCountry(@RequestBody Country country) {

        countryRepository.save(country);

        return country;
    }

    @DeleteMapping("/{id}")
    public String deleteCountry(@PathVariable("id") Integer id) {

        Optional<Country> theCountry = countryRepository.findById(id);

        if (theCountry.isEmpty()) {
            throw new RuntimeException("Country id not found - " +id);
        }

        countryRepository.deleteById(id);

        return "Deleted country id " + id;
    }
}
