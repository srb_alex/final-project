package com.sda.proiectfinalonlineshopapp.controllers;

import com.sda.proiectfinalonlineshopapp.dto.MessageResponse;
import com.sda.proiectfinalonlineshopapp.dto.ProductCategory;
import com.sda.proiectfinalonlineshopapp.models.Category;
import com.sda.proiectfinalonlineshopapp.models.Product;
import com.sda.proiectfinalonlineshopapp.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Product> getProduct(@PathVariable("id") Long id) {

        Optional<Product> theProduct = productRepository.findById(id);

        if (theProduct == null) {
            throw new RuntimeException("Product id not found - " + id);
        }

        return theProduct;
    }

    @GetMapping("/searchByCategory{id}")
    public List<Product> getProductByCategory(@PathVariable("id") Long id) {

        List<Product> theProduct = productRepository.findByCategoryId(id);

        if (theProduct == null) {
            throw new RuntimeException("Product id not found - " + id);
        }

        return theProduct;
    }

    @GetMapping("/searchByName/{id}")
    public List<Product> getProductName(@PathVariable("id") String name) {

        List<Product> theProductName = productRepository.findByNameContaining(name);

        if (theProductName == null) {
            throw new RuntimeException("Product name id not found - " + name);
        }

        return theProductName;
    }

    @PostMapping
    public ResponseEntity<?> addProduct(@Valid @RequestBody ProductCategory productCategory) {

        Product product = new Product(
                productCategory.getName(),
                productCategory.getDescription(),
                productCategory.getUnitPrice(),
                productCategory.getImageUrl(),
                productCategory.getUnitsInStock());

        Category category = productCategory.getCategory();
        product.setCategory(category);

        productRepository.save(product);

        return ResponseEntity.ok(new MessageResponse("Product added successfully!"));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateProduct(
            @PathVariable(value = "id") Long prodId,
            @Valid @RequestBody ProductCategory productCategory)  {
        Product product = productRepository.findById(prodId)
                .orElseThrow(() -> new RuntimeException("Prod not found on :: " + prodId));

        product.setDescription(productCategory.getDescription());
        product.setImageUrl(productCategory.getImageUrl());
        product.setName(productCategory.getName());
        product.setUnitPrice(productCategory.getUnitPrice());
        product.setUnitsInStock(productCategory.getUnitsInStock());

        Category category = productCategory.getCategory();
        product.setCategory(category);

        final Product updatedProd = productRepository.save(product);
        return ResponseEntity.ok(updatedProd);
    }

    @DeleteMapping("/{id}")
    public String deleteProduct(@PathVariable Long id) {

        Optional<Product> tempProduct = productRepository.findById(id);

        if (tempProduct == null) {
            throw new RuntimeException("Product id not found - " + id);
        }

        productRepository.deleteById(id);

        return "Deleted product id - " + id;
    }
}
