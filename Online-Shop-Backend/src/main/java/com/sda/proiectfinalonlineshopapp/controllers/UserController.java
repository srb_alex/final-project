package com.sda.proiectfinalonlineshopapp.controllers;

import com.sda.proiectfinalonlineshopapp.dto.LoginRequest;
import com.sda.proiectfinalonlineshopapp.dto.MessageResponse;
import com.sda.proiectfinalonlineshopapp.dto.Response;
import com.sda.proiectfinalonlineshopapp.dto.SignupRequest;
import com.sda.proiectfinalonlineshopapp.models.Role;
import com.sda.proiectfinalonlineshopapp.models.User;
import com.sda.proiectfinalonlineshopapp.repository.RoleRepository;
import com.sda.proiectfinalonlineshopapp.repository.UserRepository;
import com.sda.proiectfinalonlineshopapp.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new Response(userDetails.getId(),
                userDetails.getUsername(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        User user = new User(signUpRequest.getUsername(),
               encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findById(1L)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @GetMapping("/{id}")
    public Optional<User> getUser(@PathVariable("id") Long id) {
        Optional<User> user = userRepository.findById(id);

        if (user == null) {
            throw new RuntimeException("User id not found " + id);
        }

        return user;
    }

    @GetMapping()
    public List<User> getAllUser() {
        return userRepository.findAll();
    }


    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(
            @PathVariable(value = "id") Long userId,
            @Valid @RequestBody User userDetails) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User not found on :: " + userId));

        user.setUsername(userDetails.getUsername());
        user.setPassword(userDetails.getPassword());

        Set<Role> roles = userDetails.getRoles();
        user.setRoles(roles);

        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        Optional<User> theUser = userRepository.findById(id);

        if (theUser == null) {
            throw new RuntimeException("User id not found: " + id);
        }

        userRepository.deleteById(id);

        return "Deleted user id - " + id;
    }
}

