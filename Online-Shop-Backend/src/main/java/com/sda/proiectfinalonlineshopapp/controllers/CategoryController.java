package com.sda.proiectfinalonlineshopapp.controllers;

import com.sda.proiectfinalonlineshopapp.models.Category;
import com.sda.proiectfinalonlineshopapp.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryRepository categoryRepository;

    @Autowired
    CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Category> findCategory(@PathVariable Long id) {

        Optional<Category> theCategory = categoryRepository.findById(id);

        if (theCategory == null) {
            throw new RuntimeException("Category id not found - " + id);
        }

        return theCategory;
    }

    @PostMapping
    public Category addCategory(@RequestBody Category category) {

        categoryRepository.save(category);

        return category;
    }

    @PutMapping
    public Category updateCategory(@RequestBody Category category) {

        categoryRepository.save(category);

        return category;
    }

    @DeleteMapping("/{id}")
    public String deleteCategory(@PathVariable Long id) {

        Optional<Category> tempCategory = categoryRepository.findById(id);

        if (tempCategory == null) {
            throw new RuntimeException("Category id not found - " + id);
        }

        categoryRepository.deleteById(id);

        return "Deleted category id - " + id;
    }
}

