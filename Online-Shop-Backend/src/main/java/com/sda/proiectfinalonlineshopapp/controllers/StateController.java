package com.sda.proiectfinalonlineshopapp.controllers;

import com.sda.proiectfinalonlineshopapp.dto.MessageResponse;
import com.sda.proiectfinalonlineshopapp.dto.StateCountry;
import com.sda.proiectfinalonlineshopapp.models.Country;
import com.sda.proiectfinalonlineshopapp.models.State;
import com.sda.proiectfinalonlineshopapp.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/states")
public class StateController {

    @Autowired
    private StateRepository stateRepository;

    @GetMapping
    public List<State> findAll() {
        return stateRepository.findAll();
    }

    @GetMapping("/findByCountryId/{id}")
    public List<State> getStateByCountryName(@PathVariable("id") Integer id) {

        List<State> theState = stateRepository.findByCountryId(id);

        if(theState == null) {
            throw new RuntimeException("State id not found - "+id);
        }

        return theState;
    }

    @GetMapping("/{id}")
    public Optional<State> getState(@PathVariable("id") Integer id) {
        Optional<State> theState = stateRepository.findById(id);

        if(theState.isEmpty()) {
            throw new RuntimeException("State id not found - " +id);
        }

        return theState;
    }

    @PostMapping
    public ResponseEntity<?> addState(@Valid @RequestBody StateCountry stateCountry) {

        State state = new State(
                stateCountry.getId(),
                stateCountry.getName()
        );

        Country country = stateCountry.getCountry();

        state.setCountry(country);

        stateRepository.save(state);

        return ResponseEntity.ok(new MessageResponse("State added successful!"));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateState( @PathVariable("id") Integer id, @Valid @RequestBody StateCountry stateCountry) {

        State state = stateRepository.findById(id).orElseThrow(() -> new RuntimeException("State not found on:: " + id));

        state = new State(
                stateCountry.getId(),
                stateCountry.getName()
        );

        Country country = stateCountry.getCountry();

        state.setCountry(country);

        stateRepository.save(state);

        return ResponseEntity.ok(new MessageResponse("State added successful!"));
    }

    @DeleteMapping("/{id}")
    public String deleteState(@PathVariable("id") Integer id) {

        Optional<State> theState = stateRepository.findById(id);

        if(theState.isEmpty()) {
            throw new RuntimeException("State id not found - " +id);
        }

        stateRepository.deleteById(id);

        return  "Deleted state id - " + id;
    }

}
